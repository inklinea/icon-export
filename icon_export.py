#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Icon Export - Some .ico / .png exporting
# An Inkscape 1.2+ extension
##############################################################################

import inkex
from inkex import PathElement, Style, Group

from inklinea import Inklin

import uuid, os, copy, sys, shutil
from copy import deepcopy

from PIL import Image, ImageOps


def export_icons(self, png_export_filepath, my_object_id, export_folder_path):
    im = Image.open(png_export_filepath)

    # my_ico_image = ImageOps.fit(im, size=(500, 500))
    my_ico_image = im

    ico_size_list = [(16, 16), (24, 24), (32, 32), (48, 48), (64, 64), (128, 128), (256, 256)]

    ico_filename = my_object_id + '.ico'
    ico_filepath = os.path.join(export_folder_path, ico_filename)

    if self.options.output_format_radio == 'ico' or self.options.output_format_radio == 'ico_png':

        my_ico_image.save(ico_filepath, sizes=ico_size_list)

    if self.options.output_format_radio == 'png' or self.options.output_format_radio == 'ico_png':

        for size in ico_size_list:

            if self.options.png_fit_radio == 'pad':
                my_thumbnail = ImageOps.pad(deepcopy(im), (500, 500), method=Image.BICUBIC, color=None, centering=(0.5, 0.5))
            elif self.options.png_fit_radio == 'original':
                my_thumbnail = deepcopy(im)
            elif self.options.png_fit_radio == 'stretch':
                my_thumbnail = deepcopy(im).resize((500, 500), resample=Image.BICUBIC)

            my_thumbnail.thumbnail(size)

            size_str = '_'.join([str(item) for item in size])

            my_thumbnail_filename = f'{my_object_id}_{size_str}.png'
            my_thumbnail_filepath = os.path.join(export_folder_path, my_thumbnail_filename)
            my_thumbnail.save(my_thumbnail_filepath)

    im.close()


def make_filepaths(svg, temp_folder):

    ico_svg_filename = str(uuid.uuid4()) + '.svg'
    ico_svg_filepath = os.path.join(temp_folder, ico_svg_filename)

    png_export_filename = str(uuid.uuid4()) + '.png'
    png_export_filepath = os.path.join(temp_folder, png_export_filename)

    with open(ico_svg_filepath, 'w') as output_svg:
        ico_svg_string = svg.tostring().decode('utf-8')
        output_svg.write(ico_svg_string)

    return ico_svg_filepath, png_export_filepath


class IcoExport(inkex.EffectExtension):

    def add_arguments(self, pars):

        pars.add_argument("--main_notebook", type=str, dest="main_notebook", default=0)

        pars.add_argument("--output_folder_path", type=str, dest="output_folder_path", default=None)

        pars.add_argument("--iconify_radio", type=str, dest="iconify_radio", default='selected')

        pars.add_argument("--output_format_radio", type=str, dest="output_format_radio", default='ico')

        pars.add_argument("--png_fit_radio", type=str, dest="png_fit_radio", default='pad')


    def effect(self):

        selection_list = self.svg.selected
        if len(selection_list) < 1:
            inkex.errormsg('Nothing Selected')
            return

        output_folder_path = self.options.output_folder_path

        Inklin.check_output_filepath(self, output_folder_path, 'test')

        timestamp = Inklin.make_timestamp(self)

        export_folder_path = os.path.join(output_folder_path, timestamp)
        os.mkdir(export_folder_path)

        temp_folder = Inklin.make_temp_folder(self)

        options_list = ''

        if self.options.iconify_radio == 'canvas':

            ico_svg_filepath, png_export_filepath = make_filepaths(self.svg, temp_folder)

            action_list = f'export-area-page;export-dpi:300;export-filename:{png_export_filepath};export-do'

            export_svg, stdout = Inklin.inkscape_command_call(self, ico_svg_filepath, options_list, action_list)

            export_svg.close()

            export_icons(self, png_export_filepath, 'canvas', export_folder_path)

        elif self.options.iconify_radio == 'group_selected':

            id_list = [item.get_id() for item in selection_list]
            id_list = ','.join(id_list)
            
            ico_svg_filepath, png_export_filepath = make_filepaths(self.svg, temp_folder)

            action_list = f'select-by-id:{id_list};object-to-path;select-invert:no-layers;delete;select-all;fit-canvas-to-selection;export-dpi:300;export-filename:{png_export_filepath};export-do'

            export_svg, stdout = Inklin.inkscape_command_call(self, ico_svg_filepath, options_list, action_list)

            export_svg.close()

            export_icons(self, png_export_filepath, 'group', export_folder_path)

        elif self.options.iconify_radio == 'selected':

            for selected in selection_list:

                children = True
                my_object = selected
                my_object_id = my_object.get_id()

                if children == True:
                    ico_object = deepcopy(my_object)
                else:
                    ico_object = deepcopy(Inklin.get_pruned_object_tree(self, my_object))

                ico_object_id = str(uuid.uuid4())

                svg_base = deepcopy(Inklin.xslt_prune_object(self, self.svg))

                ico_object.set('id', ico_object_id)

                svg_base.append(ico_object)

                ico_svg_filepath, png_export_filepath = make_filepaths(svg_base, temp_folder)

                action_list = f'select-by-id:{ico_object_id};object-to-path;fit-canvas-to-selection;export-dpi:300;export-filename:{png_export_filepath};export-do'

                export_svg, stdout = Inklin.inkscape_command_call(self, ico_svg_filepath, options_list, action_list)

                export_svg.close()

                export_icons(self, png_export_filepath, my_object_id, export_folder_path)

        # Cleanup temp folder
        if hasattr(self, 'inklin_temp_folder'):
            shutil.rmtree(self.inklin_temp_folder)

if __name__ == '__main__':
    IcoExport().run()
