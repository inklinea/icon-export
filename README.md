# Icon Export

Some Simple Icon Exporting

Appears under Extensions>Export>Icon Export

▶ Batch exporting of .ico and .png
▶ Choice of padding / stretching or not
▶ Inkscape 1.2+
